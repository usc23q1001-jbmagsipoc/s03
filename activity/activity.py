year = int(input("Please input a year: \n"))

if year == 0 or year < 0:
	print("Please enter a non-zero positive year.")
elif year%4 == 0:
	print(f"{year} is a leap year.")
else:
	print(f"{year} is not a leap year.")

row = int(input("Enter number of rows: \n"))
column = int(input("Enter number of columns: \n"))

for x in range(row):
	for y in range(column):
		print("*", end = '')
	print()