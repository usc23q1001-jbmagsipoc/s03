# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course.")

# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# test_num = 75

# if test_num >= 60:
# 	print("Test passed.")
# else:
# 	print("Test failed.")

# test_num2 = int(input("Please enter the 2nd number: \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0
# 	print("The number is zero")
# else:
# 	print("The number is negative")

#Mini-Exercise:

# number = int(input("Please enter a number: \n"))

# if (number%5 == 0) and (number%3 == 0):
# 	print("The number is divisible by both 3 and 5")
# elif number%3 == 0:
# 	print("The number is divisible by 3")
# elif number%5 == 0:
# 	print("The number is divisible by 5")
# else:
# 	print("The number is not divisible by 3 nor 5")

# i = 1
# while i < 5
# 	print(f"Current Count {i}")
# 	i += 1

# #iterating over a sequence
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)

#range method

# for x in range(6):
# 	print(f"The current value is {x}")

# for x in range(6 , 10):
# 	print(f"The current value is {x}")

# for x in range(6 , 10, 2):
# 	print(f"The current value is {x}")

#Break statement
# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1

#Continue statement
j = 1
while j < 6:
	j += 1
	if j == 3:
		continue
	print(j)
	
	